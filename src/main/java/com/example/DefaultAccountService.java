package com.example;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service
public class DefaultAccountService implements AccountService {

    @Autowired
    private AccountRepository accountRepository;

    @Override
    public Account save(Account entity) {
        return accountRepository.save(entity);
    }

    @Override
    public Account getById(Serializable id) {
        return accountRepository.findOne((Long) id);
    }

    @Override
    public List<Account> getAll() {
        return accountRepository.findAll();
    }

    @Override
    public void delete(Serializable id) {
        accountRepository.delete((Long) id);
    }

}