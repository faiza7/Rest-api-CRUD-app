package com.example;

import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("/account")
public class AccountController {

    final static Logger logger = Logger.getLogger(AccountController.class);

    @Autowired
    AccountService accService;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Account> addAccount(@RequestBody Account account) {
        accService.save(account);
        logger.debug("Added:: " + account);
        return new ResponseEntity<Account>(account, HttpStatus.CREATED);
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Account> updateAccount(@RequestBody Account account) {
        Account existingAcc = accService.getById(account.getId());
        if (existingAcc == null) {
            logger.debug("Account with id " + account.getId() + " does not exists");
            return new ResponseEntity<Account>(HttpStatus.NOT_FOUND);
        } else {
            accService.save(account);
            return new ResponseEntity<Account>(HttpStatus.OK);
        }
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Account> getAccount(@PathVariable("id") Long id) {
        Account account = accService.getById(id);
        if (account == null) {
            logger.debug("Account with id " + id + " does not exists");
            return new ResponseEntity<Account>(HttpStatus.NOT_FOUND);
        }
        logger.debug("Found Account:: " + account);
        return new ResponseEntity<Account>(account, HttpStatus.OK);
    }


    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Account>> getAllAccounts() {
        List<Account> accounts = accService.getAll();
        if (accounts.isEmpty()) {
            logger.debug("Accounts does not exists");
            return new ResponseEntity<List<Account>>(HttpStatus.NO_CONTENT);
        }
        logger.debug("Found " + accounts.size() + " Accounts");
        logger.debug(Arrays.toString(accounts.toArray()));
        return new ResponseEntity<List<Account>>(accounts, HttpStatus.OK);
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteAccount(@PathVariable("id") Long id) {
        Account account = accService.getById(id);
        if (account == null) {
            logger.debug("Account with id " + id + " does not exists");
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        } else {
            accService.delete(id);
            logger.debug("Account with id " + id + " deleted");
            return new ResponseEntity<Void>(HttpStatus.GONE);
        }
    }

}
